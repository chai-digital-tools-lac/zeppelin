docker rm zeppelin

docker run \
  -d \
  -u $(id -u) \
  -p 8080:8080 \
  --name zeppelin \
  -v $PWD/zeppelin-notebook:/opt/zeppelin/notebook \
  -v $PWD/zeppelin-conf:/opt/zeppelin/conf \
  -v $PWD/zeppelin-logs:/opt/zeppelin/logs \
  -e ZEPPELIN_NOTEBOOK_CRON_ENABLE=true \
  apache/zeppelin:0.10.0
